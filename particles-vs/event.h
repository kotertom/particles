#ifndef EVENT_H
#define EVENT_H

#include <list>

template<class SenderT = void*, class EventArgsT = void*>
using EventHandler = void(*)(SenderT*, EventArgsT*);


template<class SenderT = void*, class EventArgsT = void*>
class Event
{
private:
	std::list<EventHandler<SenderT, EventArgsT>> callbacks;

	public:
	Event();
	~Event();

public:
	void operator+=(EventHandler<SenderT, EventArgsT> callback);
	void operator-=(EventHandler<SenderT, EventArgsT> callback);

	void raise(SenderT* sender, EventArgsT* args = nullptr);
	void operator()(SenderT* sender, EventArgsT* args = nullptr);
};

//template<class SenderT>
//class Event<SenderT, void> : Event<SenderT, void*>
//{
//public:
//
//};

#include "event.cpp"

#endif