#include <GL/glew.h>
#ifndef PARTICLE_H
#define PARTICLE_H

#ifdef __CUDACC__
#define CUDA_CALLABLE_MEMBER __host__ __device__
#else
#define CUDA_CALLABLE_MEMBER
#endif 

#include <thrust/host_vector.h>
#include <thrust/device_vector.h>

#include <cuda_gl_interop.h>

#define PERFORMANCE_THRESHOLD 32
#define MAX_TPB 256
#define MAX_BLOCKS 256
#define MAX_THREADS MAX_TPB * MAX_BLOCKS

struct Vec2
{
	float x;
	float y;

	__host__ __device__ Vec2 operator+(Vec2 other);
	__host__ __device__ Vec2 operator-(Vec2 other);
	__host__ __device__ Vec2 operator/(float other);
	__host__ __device__ Vec2 operator*(float other);
	__host__ __device__ Vec2 operator-();

	__host__ __device__ Vec2();
	__host__ __device__ Vec2(float x, float y);

	__host__ __device__ float length2();
	__host__ __device__ float dist2(Vec2 other);
	__host__ __device__ Vec2 normalized();
	__host__ __device__ float dot(Vec2 other);
};

class Particle {
private:
//	float timeElapsed;

public:
	Vec2 position;
	float mass;
	float radius;
	Vec2 velocity;
	Vec2 force;
//	float lifeSpan;
//	bool expire;
	float4 color;
	float restitution;
	float friction;
	float drag;

	__host__ __device__ void addForce(Vec2 force);
	__host__ __device__ Vec2 getAcceleration();

	__host__ __device__ void render();
	__host__ __device__ void update();

	__host__ __device__ Particle();
	__host__ __device__ ~Particle();
};


class ParticleSystem {
private:
	thrust::host_vector<Particle> particles;
	Vec2 initialForce;
	Vec2 initialAcceleration;
	float lastParticleGeneratedTimestamp;

public:
	Vec2 position;
	void addInitialForce(Vec2 force);
	void setInitialForce(Vec2 force);
	Vec2 getInitialForce();

	void setInitialAcceleration(Vec2 acceleration);

	void generateParticle(Particle& p, Vec2 initialVelocity);

	void update();
	void render();

	int getNumParticles()
	{
		return particles.size();
	}

	bool generateParticlesOverTime;
	float particleGenerationPeriod;


	GLuint particlePositionsBuffer;
	GLuint particleColorsBuffer;
	cudaGraphicsResource_t particlePosCGRes;
	cudaGraphicsResource_t particleColCGRes;

	ParticleSystem();
	~ParticleSystem();
};

#endif // !PARTICLE_H

//typedef struct {
//	int dim;
//	float mass;
//	float radius;
//	float* position;
//	float* velocity;
//	float* force;
//} Particle;
//
//typedef struct {
//	int numParticles;
//	Particle* particles;
//} ParticleSystem;
//
//void addForce(Particle* p, float* force);
//void addForce(ParticleSystem* system, float* force);
