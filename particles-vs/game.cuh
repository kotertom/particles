#ifndef GAME_CUH
#define GAME_CUH

#include <SDL.h>
// OpenGL / glew Headers
#define GL3_PROTOTYPES 1
#include <GL/glew.h>

#include "event.h"
#include "particle.cuh"

#include <ctime>

__constant__ const double fixedDeltaT = 1.0 / 60;

class Game {
private:
	bool running;

public:
	void run();

	ParticleSystem* particleSystem;

	Event<Game> init;
	Event<Game> handleInput;
	Event<Game> update;
	Event<Game> render;

	SDL_Window* gameWindow;
	SDL_GLContext glCtx;
	void stop();

	Game();
	~Game();
};

#endif // !GAME_CUH




