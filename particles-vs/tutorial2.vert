#version 330 core

layout(location = 0) in vec2 in_Position;
layout(location = 1) in vec4 in_Color;
// layout(location = 2) in vec2 in_TexUV;

uniform mat4 pMatrix;
 
out vec4 v_Color;

uniform sampler2D myTextureSampler;

void main(void) {
    gl_Position = pMatrix * vec4(in_Position.x, in_Position.y, 0.0, 1.0);
 
    v_Color = in_Color;
}