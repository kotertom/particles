#include "game.cuh"
#include <iostream>
#include <ctime>
#include <chrono>
#include <cuda_gl_interop.h>


void Game::run()
{
	init(this, nullptr);

	clock_t lastRender = clock();
	clock_t lastUpdate = lastRender;

	clock_t totalTicks = 0;
	unsigned long totalFrames = 0;

	running = true;
	update(this);

	while (running)
	{

		clock_t tFrame = clock();
		clock_t nextUpdate = lastUpdate + fixedDeltaT * CLOCKS_PER_SEC;

		handleInput(this);

		double timeSinceLastUpdate = double(tFrame - lastUpdate) / CLOCKS_PER_SEC;

		for(double timeRemaining = timeSinceLastUpdate; timeRemaining >= fixedDeltaT; timeRemaining -= fixedDeltaT)
		{
			lastUpdate += fixedDeltaT * CLOCKS_PER_SEC;

			update(this);
		}
		
		glClear(GL_COLOR_BUFFER_BIT);
		render(this);
		SDL_GL_SwapWindow(gameWindow);

		
		totalFrames++;
		totalTicks += tFrame - lastRender;
		printf("Avg fps: %.1f\n", totalFrames / (totalTicks / double(CLOCKS_PER_SEC)));

		lastRender = tFrame;
	}
}

void Game::stop()
{
	running = false;
	std::cout << "stopping game\n";
}

Game::Game()
{
}

Game::~Game()
{
	delete particleSystem;
}


