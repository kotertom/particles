//// Headerphile.com OpenGL Tutorial part 1
// A Hello World in the world of OpenGL ( creating a simple windonw and setting background color )
// Source code is an C++ adaption / simplicication of : https://www.opengl.org/wiki/Tutorial1:_Creating_a_Cross_Platform_OpenGL_3.2_Context_in_SDL_(C_/_SDL)
// Compile : clang++ main.cpp -lGL -lSDL2 -std=c++11 -o Test

// C++ Headers
#include <string>
#include <iostream>

// OpenGL / glew Headers
#define GL3_PROTOTYPES 1
#include <GL/glew.h>

// SDL2 Headers
#include <SDL.h>

// project headers
#include "game.cuh"
#include "particle.cuh"

// cuda
#include <cuda.h>
#include <cudaGL.h>
#include <cuda_gl_interop.h>


#include "shader.h"

std::string programName = "2D particle simulation";
Shader shader;

// Our SDL_Window ( just like with SDL2 wihout OpenGL)
SDL_Window *mainWindow;
float windowWidth = 800;
float windowHeight = 600;

// Our opengl context handle
SDL_GLContext mainContext;

bool SetOpenGLAttributes();
void PrintSDL_GL_Attributes();
void CheckSDLError(int line);
void Cleanup();

void gameInit(Game* sender);
void gameHandleInput(Game* sender);
void gameUpdate(Game* sender);
void gameRender(Game* sender);


typedef struct myVertex
{
	float x;
	float y;
	float r;
	float g;
	float b;
	float a;

} MyVertex;


bool Init()
{
	// Initialize SDL's Video subsystem
	if (SDL_Init(SDL_INIT_VIDEO) < 0)
	{
		std::cout << "Failed to init SDL\n";
		return false;
	}

	// Create our window centered at 512x512 resolution
	mainWindow = SDL_CreateWindow(programName.c_str(), SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
		windowWidth, windowHeight, SDL_WINDOW_OPENGL);
	
	// Check that everything worked out okay
	if (!mainWindow)
	{
		std::cout << "Unable to create window\n";
		CheckSDLError(__LINE__);
		return false;
	}

	// Create our opengl context and attach it to our window
	mainContext = SDL_GL_CreateContext(mainWindow);

	SetOpenGLAttributes();

	// This makes our buffer swap syncronized with the monitor's vertical refresh
	SDL_GL_SetSwapInterval(1);

	// Init GLEW
	// Apparently, this is needed for Apple. Thanks to Ross Vander for letting me know
#ifndef __APPLE__
	glewExperimental = GL_TRUE;
	GLenum glewErr = glewInit();
	if(glewErr != GLEW_OK)
	{
		fprintf(stderr, "Error: %s\n", glewGetErrorString(glewErr));
	}
	fprintf(stdout, "Status: Using GLEW %s\n", glewGetString(GLEW_VERSION));
#endif

	if (!shader.Init())
		return false;

	shader.UseProgram();

	return true;
}

bool SetOpenGLAttributes()
{
	// Set our OpenGL version.
	// SDL_GL_CONTEXT_CORE gives us only the newer version, deprecated functions are disabled
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);

	// 3.2 is part of the modern versions of OpenGL, but most video cards whould be able to run it
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);

	// Turn on double buffering with a 24bit Z buffer.
	// You may need to change this to 16 or 32 for your system
	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);

	return true;
}

int main(int argc, char *argv[])
{
	if (!Init()) {
		fprintf(stderr, "could not init OpenGL\n");
		return EXIT_FAILURE;
	}

	cudaError_t cudaStatus;

	cudaStatus = cudaSetDevice(0);
	if(cudaStatus != cudaSuccess)
	{
		fprintf(stderr, "could not set cuda device\n");
		return EXIT_FAILURE;
	}
	cudaStatus = cudaGLSetGLDevice(0);
	if (cudaStatus != cudaSuccess)
	{
		fprintf(stderr, "could not set cuda gl device\n");
		return EXIT_FAILURE;
	}

	PrintSDL_GL_Attributes();

	glDisable(GL_CULL_FACE);
	glDisable(GL_DEPTH_TEST);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable(GL_BLEND);
	glDisable(GL_ALPHA_TEST);
		

	 //Clear our buffer with a black background
	 //This is the same as :
	 //		SDL_SetRenderDrawColor(&renderer, 255, 0, 0, 255);
	 //		SDL_RenderClear(&renderer);
	
	glClearColor(0.2, 0.2, 0.2, 1.0);
	glClear(GL_COLOR_BUFFER_BIT);
	SDL_GL_SwapWindow(mainWindow);


	Game game;
	game.gameWindow = mainWindow;
	game.glCtx = SDL_GL_GetCurrentContext();
	game.init += EventHandler<Game>(&gameInit);
	game.handleInput += EventHandler<Game>(&gameHandleInput);
	game.update += EventHandler<Game>(&gameUpdate);
	game.render += EventHandler<Game>(&gameRender);
	game.run();

	Cleanup();

	return EXIT_SUCCESS;
}

float nextRandSymNormFloat()
{
	return (float(rand()) / RAND_MAX - 0.5f) * 2.0f;
}

float nextRandNormFloat()
{
	return float(rand()) / RAND_MAX;
}

void gameInit(Game* sender)
{
	std::cout << "Initializing simulation\n";

	sender->particleSystem = new ParticleSystem();
	sender->particleSystem->position = Vec2(-100, 100);
	sender->particleSystem->setInitialAcceleration(Vec2(0, -9.81f));

	srand(time(NULL));
	for(int i = 0; i < 1000; i++)
	{
		Particle p;
		p.color = { nextRandNormFloat(), nextRandNormFloat(), nextRandNormFloat(), 1 };
		p.radius = 5;
		p.mass = nextRandNormFloat() * .1f;
		p.restitution = .8;
		p.friction = 0.9;
		p.drag = 0.00001;
		sender->particleSystem->generateParticle(p, Vec2(nextRandNormFloat() * 100, -nextRandNormFloat() * 10));
	}
}

void gameHandleInput(Game* sender)
{
	SDL_Event event;
	while (SDL_PollEvent(&event))
	{
		if (event.type == SDL_QUIT)
			sender->stop();

		if (event.type == SDL_KEYDOWN)
		{
			switch (event.key.keysym.sym)
			{
			case SDLK_ESCAPE:
				sender->stop();
				break;
			case SDLK_r:
				// Cover with red and update
				glClearColor(1.0, 0.0, 0.0, 1.0);
				glClear(GL_COLOR_BUFFER_BIT);
				SDL_GL_SwapWindow(mainWindow);
				break;
			case SDLK_g:
				// Cover with green and update
				glClearColor(0.0, 1.0, 0.0, 1.0);
				glClear(GL_COLOR_BUFFER_BIT);
				SDL_GL_SwapWindow(mainWindow);
				break;
			case SDLK_b:
				// Cover with blue and update
				glClearColor(0.0, 0.0, 1.0, 1.0);
				glClear(GL_COLOR_BUFFER_BIT);
				SDL_GL_SwapWindow(mainWindow);
				break;
			default:
				break;
			}
		}
	}
}

void gameUpdate(Game* sender)
{
	sender->particleSystem->update();
}

void gameRender(Game* sender)
{
	sender->particleSystem->render();
}

void Cleanup()
{
	// Delete our OpengL context
	SDL_GL_DeleteContext(mainContext);

	// Destroy our window
	SDL_DestroyWindow(mainWindow);

	// Shutdown SDL 2
	SDL_Quit();
}

void CheckSDLError(int line = -1)
{
	std::string error = SDL_GetError();

	if (error != "")
	{
		std::cout << "SLD Error : " << error << std::endl;

		if (line != -1)
			std::cout << "\nLine : " << line << std::endl;

		SDL_ClearError();
	}
}

void PrintSDL_GL_Attributes()
{
	int value = 0;
	SDL_GL_GetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, &value);
	std::cout << "SDL_GL_CONTEXT_MAJOR_VERSION : " << value << std::endl;

	SDL_GL_GetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, &value);
	std::cout << "SDL_GL_CONTEXT_MINOR_VERSION: " << value << std::endl;
}