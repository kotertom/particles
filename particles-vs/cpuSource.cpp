//#include <stdio.h>
//#include <SDL.h>
//#include <GL/glew.h>
//#include <stdlib.h>
//#include <iostream>
//
//#define _CRT_SECURE_NO_WARNINGS
//
//// GLOBALS
//
//std::string programName = "2D particle simulation";
//
//SDL_Window *mainWindow;
//float windowWidth = 800;
//float windowHeight = 600;
//
//SDL_GLContext mainContext;
//
//
//// UTILS
//
//#define MAX_ERR_MSG 256
//#define PERROR(desc) ( fprintf(stderr, "%s, file: %s, line: %d\n", desc, __FILE__, __LINE__) )
//typedef struct
//{
//	int error;
//	char errorMessage[MAX_ERR_MSG];
//} FunctionResult;
//
//void printError(FunctionResult res)
//{
//	if(res.error != 0)
//	{
//		fprintf(stderr, "%s\n", res.errorMessage);
//		exit(EXIT_FAILURE);
//	}
//		
//}
//
//
//// FUNCTION LIST
//
//FunctionResult init();
//FunctionResult cleanup();
//
//int main()
//{
//	printError(init());
//
//
//	printError(cleanup());
//
//	return EXIT_SUCCESS;
//}
//
//
//FunctionResult init()
//{
//	FunctionResult ret;
//	ret.error = 0;
//
//	// Initialize SDL's Video subsystem
//	if (SDL_Init(SDL_INIT_VIDEO) < 0)
//	{
//		ret.error = 1;
//		PERROR("Failed to init SDL");
//		strcpy(ret.errorMessage, "Failed to init SDL");
//		return ret;
//	}
//
//	// Create our window centered at 512x512 resolution
//	mainWindow = SDL_CreateWindow(programName.c_str(), SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
//		windowWidth, windowHeight, SDL_WINDOW_OPENGL);
//
//	// Check that everything worked out okay
//	if (!mainWindow)
//	{
//		ret.error = 1;
//		PERROR(SDL_GetError());
//		strcpy(ret.errorMessage, "Unable to create window");
//		return ret;
//	}
//
//	// Create our opengl context and attach it to our window
//	mainContext = SDL_GL_CreateContext(mainWindow);
//
//	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
//	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
//	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
//	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
//
//	// This makes our buffer swap syncronized with the monitor's vertical refresh
//	SDL_GL_SetSwapInterval(1);
//
//	// Init GLEW
//	// Apparently, this is needed for Apple. Thanks to Ross Vander for letting me know
//#ifndef __APPLE__
//	glewExperimental = GL_TRUE;
//	GLenum glewErr = glewInit();
//	if (glewErr != GLEW_OK)
//	{
//		PERROR(glewGetErrorString(glewErr));
////		fprintf(stderr, "Error: %s\n", glewGetErrorString(glewErr));
//	}
//	fprintf(stdout, "Status: Using GLEW %s\n", glewGetString(GLEW_VERSION));
//#endif
//
//}
//
//FunctionResult cleanup()
//{
//	FunctionResult ret;
//	ret.error = 0;
//	// Delete our OpengL context
//	SDL_GL_DeleteContext(mainContext);
//
//	// Destroy our window
//	SDL_DestroyWindow(mainWindow);
//
//	// Shutdown SDL 2
//	SDL_Quit();
//
//	return ret;
//}