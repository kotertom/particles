
#include "particle.cuh"
#include "game.cuh"
#include <device_launch_parameters.h>
#include <algorithm>
#include <cuda_gl_interop.h>
#include "lodepng.h"

#define EPS 1e-8
#define ARENA_W 300
#define ARENA_H 300

#define gpuErrchk(ans) { gpuAssert((ans), __FILE__, __LINE__); }
inline void gpuAssert(cudaError_t code, const char *file, int line, bool abort = true)
{
	if (code != cudaSuccess)
	{
		fprintf(stderr, "GPUassert: %s %s %d\n", cudaGetErrorString(code), file, line);
		if (abort) exit(code);
	}
}


__host__ __device__ Vec2 Vec2::operator+(Vec2 other) {
	Vec2 newVec;
	newVec.x = this->x + other.x;
	newVec.y = this->y + other.y;

	return newVec;
}

Vec2 Vec2::operator-(Vec2 other)
{
	Vec2 newVec;
	newVec.x = this->x - other.x;
	newVec.y = this->y - other.y;

	return newVec;
}

__host__ __device__ Vec2 Vec2::operator/(float other)
{
	Vec2 ret;
	ret.x = this->x / other;
	ret.y = this->y / other;
	return ret;
}

__host__ __device__ Vec2 Vec2::operator*(float other)
{
	Vec2 ret;
	ret.x = this->x * other;
	ret.y = this->y * other;
	return ret;
}

__host__ __device__ Vec2 Vec2::operator-()
{
	Vec2 ret;
	ret.x = -this->x;
	ret.y = -this->y;
	return ret;
}

__host__ __device__ Vec2::Vec2(): x(0), y(0)
{
}

__host__ __device__ Vec2::Vec2(float x, float y): x(x), y(y)
{
}

__host__ __device__ float Vec2::length2()
{
	return x*x + y*y;
}

__host__ __device__ float Vec2::dist2(Vec2 other)
{
	return (*this - other).length2();
}

Vec2 Vec2::normalized()
{
	return *this / sqrt(this->length2());
}

float Vec2::dot(Vec2 other)
{
	return x*other.x + y*other.y;
}

Vec2 ParticleSystem::getInitialForce() {
	return this->initialForce;
}

void ParticleSystem::setInitialAcceleration(Vec2 acceleration)
{
	this->initialAcceleration = acceleration;
}

void ParticleSystem::generateParticle(Particle& p, Vec2 initialVelocity)
{
	p.position = this->position;
	p.velocity = initialVelocity;
	p.addForce(getInitialForce());
	p.addForce(initialAcceleration * p.mass);
	particles.push_back(p);
}




__global__ void k_update(Particle* particles, int numParticles, float2* posBuffer, float4* colBuffer)
{
	int id = blockIdx.x * blockDim.x + threadIdx.x;
	
	for(auto i = id; i < numParticles; i += MAX_THREADS)
	{
		particles[i].update();
		posBuffer[i] = { particles[i].position.x, particles[i].position.y };
		colBuffer[i] = particles[i].color;
	}
}

#define COLLISION_NO 0
#define COLLISION_CONTACT 1
#define COLLISION_YES 2

__host__ __device__ int testCollision(Particle p1, Particle p2)
{
	float r = p1.radius + p2.radius;
	float d = p1.position.dist2(p2.position) - r*r;
	if (abs(d) < EPS)
		return COLLISION_CONTACT;
	else if (d < 0)
		return COLLISION_YES;
	else
		return COLLISION_NO;
}

struct Vec2Decomposition
{
	Vec2 tangent;
	Vec2 normal;
};

__host__ __device__ Vec2Decomposition decomposeCollisionVelocity(Vec2 velocity, Vec2 normal)
{
	Vec2Decomposition dec;
	dec.normal = normal * velocity.dot(normal);
	dec.tangent = velocity - dec.normal;

	return dec;
}

__global__ void k_fixCollisions(Particle* particles, int numParticles, float2* posBuffer)
{
	// idx - first particle
	// idy - second particle
	int idx = blockIdx.x * blockDim.x + threadIdx.x;
	int idy = blockIdx.y * blockDim.y + threadIdx.y;

	int odd = numParticles % 2;
	numParticles /= 2;

	// loop for if there are not enough threads
	for(auto ix = idx; ix < numParticles; ix += MAX_THREADS)
	{
		for(auto iy = idy; iy < numParticles; iy += MAX_THREADS)
		{
			int tix, tiy;
			if(ix == iy)
			{
				return;
			}
			if(ix > iy)
			{
				tix = numParticles - ix - 1;
				tiy = numParticles - iy - 1;
			}
			else
			{
				tix = ix;
				tiy = iy;
			}
			int collisionType = testCollision(particles[tix], particles[tiy]);
			switch (collisionType)
			{
			case COLLISION_NO:
				break;
			case COLLISION_CONTACT:
				{
				Vec2 norm = (particles[tix].position - particles[tiy].position).normalized();
				Vec2Decomposition decX = decomposeCollisionVelocity(particles[tix].velocity, norm);
				Vec2Decomposition decY = decomposeCollisionVelocity(particles[tiy].velocity, -norm);

				// bounce
//				if (particles[tix].velocity.dot(norm) < 0)
//					particles[tix].velocity = particles[tix].velocity - decX.normal - decX.normal * ((particles[tix].restitution + particles[tiy].restitution) * 0.5);
//				if (particles[tiy].velocity.dot(-norm) < 0)
//					particles[tiy].velocity = particles[tiy].velocity - decY.normal - decY.normal * ((particles[tix].restitution + particles[tiy].restitution) * 0.5);

				float r = (particles[tix].restitution + particles[tiy].restitution) * 0.5;
				Vec2 c = (decX.normal * particles[tix].mass + decY.normal * particles[tiy].mass) * (1 + r) / (particles[tix].mass + particles[tiy].mass);
				particles[tix].velocity = c - decX.normal * r;
				particles[tiy].velocity = c - decY.normal * r;

				// friction
				float frac = 1.f - (particles[tix].friction + particles[tiy].friction) * 0.5f;
				particles[tix].velocity = particles[tix].velocity - decX.tangent * frac;
				particles[tiy].velocity = particles[tiy].velocity - decY.tangent * frac;

				break;
				}
			case COLLISION_YES:
				{
				Vec2 norm = (particles[tix].position - particles[tiy].position).normalized();
				Vec2Decomposition decX = decomposeCollisionVelocity(particles[tix].velocity, norm);
				Vec2Decomposition decY = decomposeCollisionVelocity(particles[tiy].velocity, -norm);

				// bounce
				if (particles[tix].velocity.dot(norm) < 0)
					particles[tix].velocity = particles[tix].velocity - decX.normal - decX.normal * ((particles[tix].restitution + particles[tiy].restitution) * 0.5);
				if (particles[tiy].velocity.dot(-norm) < 0)
					particles[tiy].velocity = particles[tiy].velocity - decY.normal - decY.normal * ((particles[tix].restitution + particles[tiy].restitution) * 0.5);

//				float r = (particles[tix].restitution + particles[tiy].restitution) * 0.5;
//				Vec2 c = (particles[tix].velocity * particles[tix].mass + particles[tiy].velocity * particles[tiy].mass) * (1 + r) / (particles[tix].mass + particles[tiy].mass);
//				particles[tix].velocity = c - particles[tix].velocity * r;
//				particles[tiy].velocity = c - particles[tiy].velocity * r;

				// friction
				float frac = 1.f - (particles[tix].friction + particles[tiy].friction) * 0.5f;
				particles[tix].velocity = particles[tix].velocity - decX.tangent * frac;
				particles[tiy].velocity = particles[tiy].velocity - decY.tangent * frac;

				break;
				}
			}

		}
	}
}

__global__ void k_fixBoundaryCollisions(Particle* particles, int numParticles, float left, float right, float top, float bottom)
{
	// idx - particle no.
	// idy - which boundary
	//		0 - left,
	//		1 - right,
	//		2 - top,
	//		3 - bottom

	int idx = blockIdx.x * blockDim.x + threadIdx.x;
	int idy = blockIdx.y * blockDim.y + threadIdx.y;

	for (auto ix = idx; ix < numParticles; ix += MAX_THREADS)
	{
		int collisionType = COLLISION_NO;
		Vec2 norm;
		switch(idy)
		{
		case 0:
		{
			if(abs(particles[ix].position.x - particles[ix].radius - left) < EPS)
			{
				collisionType = COLLISION_CONTACT;
			}
			else if(particles[ix].position.x - particles[ix].radius < left)
			{
				collisionType = COLLISION_YES;
			}

			norm = Vec2(1, 0);
			break;
		}
		case 1:
		{
			if (abs(particles[ix].position.x + particles[ix].radius - right) < EPS)
			{
				collisionType = COLLISION_CONTACT;
			}
			else if (particles[ix].position.x + particles[ix].radius > right)
			{
				collisionType = COLLISION_YES;
			}

			norm = Vec2(-1, 0);
			break;
		}
		case 2:
		{
			if (abs(particles[ix].position.y + particles[ix].radius - top) < EPS)
			{
				collisionType = COLLISION_CONTACT;
			}
			else if (particles[ix].position.y + particles[ix].radius > top)
			{
				collisionType = COLLISION_YES;
			}

			norm = Vec2(0, -1);
			break;
		}
		case 3:
		{
			if (abs(particles[ix].position.y - particles[ix].radius - bottom) < EPS)
			{
				collisionType = COLLISION_CONTACT;
			}
			else if (particles[ix].position.y - particles[ix].radius < bottom)
			{
				collisionType = COLLISION_YES;
			}

			norm = Vec2(0, 1);
			break;
		}
		}

		switch (collisionType)
		{
		case COLLISION_NO:
			break;
		case COLLISION_CONTACT:
			{
			Vec2Decomposition dec = decomposeCollisionVelocity(particles[ix].velocity, norm);
			// bounce
			if (particles[ix].velocity.dot(norm) < 0)
				particles[ix].velocity = particles[ix].velocity - dec.normal - dec.normal * particles[ix].restitution;
			// friction
			float frac = 1.f - particles[ix].friction;
			particles[ix].velocity = particles[ix].velocity - dec.tangent * frac;
			break;
			}
		case COLLISION_YES:
			{
//			Vec2Decomposition dec = decomposeCollisionVelocity(particles[ix].velocity, norm);
//			// bounce
//			particles[ix].velocity = particles[ix].velocity - dec.normal - dec.normal * particles[ix].restitution;
//			// friction
//			float frac = 1.f - particles[ix].friction;
//			particles[ix].velocity = particles[ix].velocity - dec.tangent * frac;
//			break;
			Vec2Decomposition dec = decomposeCollisionVelocity(particles[ix].velocity, norm);
			// bounce
			if (particles[ix].velocity.dot(norm) < 0)
				particles[ix].velocity = particles[ix].velocity - dec.normal - dec.normal * particles[ix].restitution;
			// friction
			float frac = 1.f - particles[ix].friction;
			particles[ix].velocity = particles[ix].velocity - dec.tangent * frac;
			break;
			}
		}
	}
}



void ParticleSystem::update()
{
	if(getNumParticles() == 0)
		return;

	cudaGraphicsResource_t particlePosCGRes, 
						   particleColCGRes;

	// register
	glBindBuffer(GL_ARRAY_BUFFER, particlePositionsBuffer);
	glBufferData(GL_ARRAY_BUFFER, getNumParticles() * sizeof(float2), NULL, GL_DYNAMIC_DRAW);
	gpuErrchk(cudaGraphicsGLRegisterBuffer(&particlePosCGRes, particlePositionsBuffer, cudaGraphicsRegisterFlagsWriteDiscard));

	glBindBuffer(GL_ARRAY_BUFFER, particleColorsBuffer);
	glBufferData(GL_ARRAY_BUFFER, getNumParticles() * sizeof(float4), NULL, GL_DYNAMIC_DRAW);
	gpuErrchk(cudaGraphicsGLRegisterBuffer(&particleColCGRes, particleColorsBuffer, cudaGraphicsRegisterFlagsWriteDiscard));

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// map
	gpuErrchk(cudaGraphicsMapResources(1, &particlePosCGRes));
	gpuErrchk(cudaGraphicsMapResources(1, &particleColCGRes));

	size_t d_posBufferSize, d_colBufferSize;
	float2* d_posBufferPtr;
	float4* d_colBufferPtr;
	gpuErrchk(cudaGraphicsResourceGetMappedPointer((void**)&d_posBufferPtr, &d_posBufferSize, particlePosCGRes));
	gpuErrchk(cudaGraphicsResourceGetMappedPointer((void**)&d_colBufferPtr, &d_colBufferSize, particleColCGRes));

	int numParticles = getNumParticles();

	assert(d_posBufferSize / sizeof(float2) == numParticles);
	assert(d_colBufferSize / sizeof(float4) == numParticles);

	// send data to device
	thrust::device_vector<Particle> d_particles = particles;

	// run update kernel
	int numBlocks = std::min(int(ceil(float(numParticles) / MAX_TPB)), MAX_BLOCKS);
	int numThreadsPerBlock = std::min(numParticles, MAX_TPB);
	k_update<<<numBlocks, numThreadsPerBlock>>>(
		thrust::raw_pointer_cast(d_particles.data()), 
		numParticles,
		d_posBufferPtr,
		d_colBufferPtr
	);



	// run collision fixing kernel
	dim3 numTPB3;
	int tpb3 = std::min(ceil(numParticles / 2.f), float(MAX_TPB));
	numTPB3.x = tpb3;
	numTPB3.y = 4;
	numTPB3.z = 1;
	dim3 numBlocks3;
	int numBlocks3i = std::min(int(ceil(ceil(float(numParticles) / 2.f) / MAX_TPB)), MAX_BLOCKS);
	numBlocks3.x = numBlocks3i;
	numBlocks3.y = 1;
	numBlocks3.z = 1;
	k_fixCollisions <<<numBlocks3, numTPB3>>>(
		thrust::raw_pointer_cast(d_particles.data()),
		numParticles,
		d_posBufferPtr
	);


	// fix collisions with boundaries
	dim3 numTPB3b;
	numTPB3b.x = numThreadsPerBlock;
	numTPB3b.y = 4;
	numTPB3b.z = 1;
	dim3 numBlocks3b;
	numBlocks3b.x = numBlocks;
	numBlocks3b.y = 1;
	numBlocks3b.z = 1;
	k_fixBoundaryCollisions <<<numBlocks3b, numTPB3b >>> (
		thrust::raw_pointer_cast(d_particles.data()),
		numParticles, 
		-ARENA_W / 2, ARENA_W / 2, ARENA_H / 2, -ARENA_H / 2
	);


	// retrieve data from device (debug)
	particles = d_particles;
//	printf("new position: %.2f,%.2f\n", particles[0].position.x, particles[0].position.y);
//	printf("new color: %.2f,%.2f,%.2f,%.2f\n", particles[0].color.x, particles[0].color.y, particles[0].color.z, particles[0].color.w);
	
	// unmap
	gpuErrchk(cudaGraphicsUnmapResources(1, &particlePosCGRes));
	gpuErrchk(cudaGraphicsUnmapResources(1, &particleColCGRes));

	// unregister
	gpuErrchk(cudaGraphicsUnregisterResource(particlePosCGRes));
	gpuErrchk(cudaGraphicsUnregisterResource(particleColCGRes));
}



void ParticleSystem::render()
{
	if(getNumParticles() == 0)
		return;

	glBindVertexArray(1);

	// particle size
	glPointSize(particles[0].radius*4);
	glEnable(GL_POINT_SMOOTH);

	// colors
	glBindBuffer(GL_ARRAY_BUFFER, particleColorsBuffer);
	glVertexAttribPointer(
		1,
		4,
		GL_FLOAT,
		GL_FALSE,
		4 * sizeof(GLfloat),
		(GLvoid*)0
	);
	glEnableVertexAttribArray(1);

	// positions
	glBindBuffer(GL_ARRAY_BUFFER, particlePositionsBuffer);
	glVertexAttribPointer(
		0,
		2,
		GL_FLOAT,
		GL_FALSE,
		2 * sizeof(GLfloat),
		(GLvoid*)0
	);
	glEnableVertexAttribArray(0);

//	int w, h;
//	SDL_GL_GetDrawableSize(SDL_GL_GetCurrentWindow(), &w, &h);
//	const GLfloat pMatrix[] {
//		2.f / w, 0, 0, 0,
//		0, 2.f / h, 0, 0,
//		0, 0, 1, 0,
//		-1, 1, 0, 1
//	};
	const GLfloat pMatrix[]{
		2.f / ARENA_W, 0, 0, 0,
		0, 2.f / ARENA_H, 0, 0,
		0, 0, 1, 0,
		0, 0, 0, 1
	};
//	printf("w,h: %d, %d\n", w, h);
	glUniformMatrix4fv(glGetUniformLocation(1, "pMatrix"), 1, GL_FALSE, pMatrix);

	glDrawArrays(GL_POINTS, 0, getNumParticles());

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glDisableVertexAttribArray(0);
	glDisableVertexAttribArray(1);
}



ParticleSystem::ParticleSystem()
{
	fprintf(stdout, "Initializing particle system\n");
	GLuint VertexArrayID;
	glGenVertexArrays(1, &VertexArrayID);
	fprintf(stderr, "Vertex array id: %d\n", VertexArrayID);
	glBindVertexArray(VertexArrayID);

	glGenBuffers(1, &particlePositionsBuffer);
	glGenBuffers(1, &particleColorsBuffer);


//	std::vector<unsigned char> textureImg;
//	unsigned width, height;
//	unsigned error = lodepng::decode(textureImg, width, height, "../particle.png");
//	if(error != 0)
//	{
//		std::cout << "error " << error << ": " << lodepng_error_text(error) << std::endl;
//	}
//	glEnable(GL_TEXTURE_2D);
//	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST); //GL_NEAREST = no smoothing
//	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
//	glTexImage2D(GL_TEXTURE_2D, 0, 4, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, textureImg.data());

	fprintf(stdout, "Finished initializing particle system\n");
}

ParticleSystem::~ParticleSystem()
{
	glDeleteBuffers(1, &particlePositionsBuffer);
	glDeleteBuffers(1, &particleColorsBuffer);
}

void ParticleSystem::setInitialForce(Vec2 force) {
	this->initialForce = force;
}

void ParticleSystem::addInitialForce(Vec2 force) {
	this->initialForce = this->initialForce + force;
}

__host__ __device__ void Particle::addForce(Vec2 force) {
	this->force = this->force + force;
}

__host__ __device__ Vec2 Particle::getAcceleration()
{
	return (force - (velocity * drag) ) / this->mass;
}

__device__ void Particle::render() {

}

__device__ void Particle::update() {
	this->position = this->position + this->velocity * fixedDeltaT;
	this->velocity = this->velocity + this->getAcceleration() * fixedDeltaT;
}

//__device__ void updateParticle(Particle* particle, double fixedDeltaT)
//{
//	particle->position = particle->position + particle->velocity * fixedDeltaT;
//	particle->velocity = particle->velocity + particle->getAcceleration() * fixedDeltaT;
//}

Particle::Particle()
{
}

Particle::~Particle()
{
}
