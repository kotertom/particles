#ifdef EVENT_H

#include "event.h"


template<class SenderT, class EventArgsT>
Event<SenderT, EventArgsT>::Event()
{
}

template<class SenderT, class EventArgsT>
Event<SenderT, EventArgsT>::~Event()
{
}

template<class SenderT, class EventArgsT>
void Event<SenderT, EventArgsT>::operator+=(EventHandler<SenderT, EventArgsT> callback)
{
	callbacks.push_back(callback);
}

template<class SenderT, class EventArgsT>
void Event<SenderT, EventArgsT>::operator-=(EventHandler<SenderT, EventArgsT> callback)
{
	callbacks.remove(callback);
}

template<class SenderT, class EventArgsT>
void Event<SenderT, EventArgsT>::raise(SenderT* sender, EventArgsT* args)
{
	for(auto callback : callbacks)
	{
		callback(sender, args);
	}
}

template<class SenderT, class EventArgsT>
void Event<SenderT, EventArgsT>::operator()(SenderT* sender, EventArgsT* args)
{
	raise(sender, args);
}

#endif