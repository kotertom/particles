#pragma once
class SceneObject
{
public:
	SceneObject();
	~SceneObject();

	virtual void update() = 0;
	virtual void render() = 0;
};

